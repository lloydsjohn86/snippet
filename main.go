// snippet allows to to create snippets on GitLab.com from STDIN or a file
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"github.com/urfave/cli"
	gitlab "github.com/xanzy/go-gitlab"
)

var (
	version = "0.4.0"

	filePath = ""
	token    = ""
)

var host string

func main() {
	app := newApp()

	err := app.Run(os.Args)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func run(ctx *cli.Context) error {
	if err := setToken(ctx); err != nil {
		return err
	}

	if token == "" {
		return fmt.Errorf("error: no token, can't post snippet")
	}

	filePath = ctx.String("file")
	permission := ctx.String("permission")

	buff, err := snippetContents(ctx)
	if err != nil {
		return err
	}

	url, err := postSnippet(string(buff), permission, token)
	if err != nil {
		return err
	}

	fmt.Println(url)
	return nil
}

func setToken(ctx *cli.Context) error {
	token = ctx.String("token")
	if token != "" {
		return nil
	}

	usr, err := user.Current()
	if err != nil {
		return err
	}

	tokenFile := filepath.Join(usr.HomeDir, ".gitlab-token")
	file, err := os.Open(tokenFile)
	if err != nil {
		return err
	}
	defer file.Close()

	buff, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	token = strings.TrimSpace(strings.SplitN(string(buff), " ", 2)[0])

	return nil
}

func snippetContents(ctx *cli.Context) ([]byte, error) {
	if ctx.NArg() == 1 {
		filePath = ctx.Args().First()
	}

	if filePath == "" {
		return ioutil.ReadAll(os.Stdin)
	}

	return ioutil.ReadFile(filePath)
}

func postSnippet(content, permission, token string) (string, error) {
	Visibility := visibiltyLevel(permission)
	title := title(&content)
	fileName := fileName(&content)

	client := gitlab.NewClient(nil, token)
	client.SetBaseURL(gitlabHost())
	snippet, _, err := client.Snippets.CreateSnippet(
		&gitlab.CreateSnippetOptions{
			Title:      &title,
			FileName:   &fileName,
			Content:    &content,
			Visibility: &Visibility,
		})

	if err != nil {
		return "", err
	}

	return snippet.WebURL, nil
}

func gitlabHost() string {
	if len(host) == 0 {
		return "gitlab.com/"

	}

	if host[len(host)-1:] == "/" {
		return host + "/"
	}

	return host
}

func visibiltyLevel(permission string) gitlab.VisibilityValue {
	switch permission {
	case "private":
		return gitlab.PrivateVisibility
	case "internal":
		return gitlab.InternalVisibility
	default:
		return gitlab.PublicVisibility
	}
}

func title(content *string) string {
	firstLine := strings.SplitN(*content, "\n", 2)[0]

	if len(firstLine) > 70 {
		return string(firstLine[:70])
	}
	return string(firstLine)
}

func fileName(content *string) string {
	if filePath != "" {
		return filepath.Base(filePath)
	}

	return title(content)
}

func newApp() *cli.App {
	app := cli.NewApp()

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "file, f",
			Usage: "Create a snippet from file on disk instead of piped input",
		},
		cli.StringFlag{
			Name:  "permission, p",
			Usage: "Set visibility of the snippet, one of: public, internal, or private",
			Value: "public",
		},
		cli.StringFlag{
			Name:   "token, t",
			Usage:  "Token if not using the $HOME/.gitlab-token file",
			EnvVar: "GITLAB_TOKEN",
		},
		cli.StringFlag{
			Name:        "host",
			Usage:       "Hostname of a GitLab istance to create the snippet on",
			Destination: &host,
			Value:       "https://gitlab.com",
		},
	}

	app.Name = "snippet"
	app.Usage = "Pipe to GitLab.com as Snippet"
	app.Action = run
	app.Version = version

	return app
}
